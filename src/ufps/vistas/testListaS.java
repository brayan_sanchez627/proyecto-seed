package ufps.vistas;

import ufps.util.colecciones_seed.ListaS;

public class testListaS {

    public static void main(String[] args) {
        ListaS l1 = new ListaS();
        ListaS l2 = new ListaS();
        
        for (int i = 0; i < 10; i++) {
            l1.insertarAlInicio((int) (Math.random() * 10));
            l2.insertarAlInicio((int) (Math.random() * 10)); 
        }
        
        System.out.println(l1.toString());
        System.out.println(l2.toString());
        System.out.println(l1.interseccionConjuntos(l2).toString());
        System.out.println(l1.diferenciaDeListas(l2).toString());
        System.out.println(l1.toString());
        System.out.println(l2.toString());
        
    }
}
